# Introduction

This example project is and example Spark job to migrate from
a JDBC source to Cassandra.  It is designed as a runable example
that can be used out-of-the-box in a general way.  It is also
provided as example code that you can modify if you need to
transform data en route to Cassandra, or if you know a more
efficient way to read from a particular relational database.

# High-level design

The goal with this Spark job is to read from a relational
source to create a Spark DataFrame, and then to use the Spark
Cassandra Connector to efficiently write the data to Cassandra.
This example takes a generic approach to reading from a
relational source.

The JDBC data source for Spark partitions the data by
specifying a column in the table of interest, and guiding
Spark in how to break the table by this column into a specified
number of partitions.  This allows for the Spark Executors to
read from the JDBC source in parallel.

The Spark Cassandra Connector handles efficientlly writing to
Cassandra in parallel.

# Configurations

This example contains a number of configuration options to
specify the JDBC source, credentials, and partitioning column.
It also contains some configuration options to specify the
destination Cassandra keyspace and table.  These can be set
from the command-line during the call to `dse spark-submit`.

## JDBC Source Options
The following are the necessary options to configure the
JDBC source:
   spark.migrator.src.url : The connection URL (e.g., jdbc:mysql://1.2.3.4:5678/mydb)
   spark.migrator.src.driver : The class name for the JDBC driver
   spark.migrator.src.username : The username for the JDBC source
   spark.migrator.src.password : The password for the JDBC source
   spark.migrator.src.table : The table of interest in the JDBC source
   spark.migrator.src.partitionColumn : The name of the column to use to make partitions
   spark.migrator.src.lowerBound : The (approximate) lower bound value on this partition column
   spark.migrator.src.upperBound : The (approximate) upper bound value on this partition column
   spark.migrator.src.numPartitions : Number of Spark partitions to make

In addition to these options, you will need the
corresponding JDBC jar for the particular database source.
This will be provided to `dse spark-submit` with a `--jar`
flag.

## Cassandra Destination Options
The following are the necessary options to configure the
Cassandra destination:
   spark.migrator.dst.keyspace : The destination keyspace in Cassandra
   spark.migrator.dst.table : The destination table in Cassandra

The host, username and password for the Cassandra database can be
set just like with the Spark Cassandra Connector:
   spark.cassandra.connection.host : Comma-separated list of IP addresses for the Cassandra cluster
   spark.cassandra.auth.username : Cassandra username
   spark.cassandra.auth.password : Cassandra password

## Additional Options
In addition, there is an option to specify which columns
from the source table are to be moved.  This is an example
of adding a transformation or filter to the flow.
   spark.migrator.src.columns : comma-separated list of columns

# Example command-line
```
   dse spark-submit --conf spark.migrator.src.url="jdbc:mysql://1.2.3.4:5678/mydb" \
                    --conf spark.migrator.src.driver="com.mysql.jdbc.Driver" \
                    --conf spark.migrator.src.username=jdbcuser \
                    --conf spark.migrator.src.password=letmein \
                    --conf spark.migrator.src.table=mysqlTable \
                    --conf spark.migrator.src.partitionColumn=columnToCut \
                    --conf spark.migrator.src.lowerBound=0 \
                    --conf spark.migrator.src.upperBound=10000 \
                    --conf spark.migrator.src.numPartitions=50 \
                    --conf spark.migrator.dst.keyspace=cassandraKeyspace \
                    --conf spark.migrator.dst.table=cassandraTable \
                    --conf spark.migrator.src.columns="columnToCut,a,b,c,d" \
		    --conf spark.cassandra.connection.host=9.8.7.6,9.8.7.5 \
		    --conf spark.cassandra.auth.username=cassuser \
		    --conf spark.cassandra.auth.password=opensesame \
                    --jar /tmp/mysql-connector-java-5.1.40-bin.jar \
                    --class hessian.Migrator \
                    migrator-assembly-0.1.jar
```

# Customization

If you would like to modify the way in which the DataFrame
reads from the JDBC source, you can modify how the fromJdbc
DataFrame is created.

If you would like to transform the data before writing to
Cassandra, you can modify the fromJdbc DataFrame before then
saving it to Cassandra.
