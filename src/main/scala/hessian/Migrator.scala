/*
 * This is an example application for migrating data from a JDBC data source
 * to DSE/Cassandra.  The approach is very generic for JDBC sources and while
 * it will result in somewhat efficient movement of data, customization based
 * on the specific relational database could result in more efficient transfers.
 *
 * This example application is intended to demonstrate how one could perform
 * this migration task, but also is a runnable example and can be used out
 * of the box.  Note, though, as stated above, one could perhaps get better
 * performance by editing this example for the specific relational database.
 *
 * All options to this application must be passed via the --conf option when
 * running "dse spark-submit".  The options that must be provided are:
 *   spark.migrator.src.url : The connection URL (e.g., jdbc:mysql://1.2.3.4:5678/mydb)
 *   spark.migrator.src.driver : The class name for the JDBC driver
 *   spark.migrator.src.username : The username for the JDBC source
 *   spark.migrator.src.password : The password for the JDBC source
 *   spark.migrator.src.table : The table of interest in the JDBC source
 *   spark.migrator.src.partitionColumn : The name of the column to use to make partitions
 *   spark.migrator.src.lowerBound : The (approximate) lower bound value on this partition column
 *   spark.migrator.src.upperBound : The (approximate) upper bound value on this partition column
 *   spark.migrator.src.numPartitions : Number of Spark partitions to make
 *
 *   spark.migrator.dst.keyspace : The destination keyspace in Cassandra
 *   spark.migrator.dst.table : The destination table in Cassandra
 *
 * You can optionally set the columns of interest by setting the following:
 *   spark.migrator.src.columns : comma-separated list of columns
 *
 * The destination Cassandra keyspace and table must be set up prior to running
 * this program.  The column names in the destination must match the columns 
 * being selected (default is all the columns) from the JDBC source.  If you need
 * to change the names of the columns, you can modify this program and use
 * normal Dataframe operations to do this.
 *
 * Additoinal Spark options can be set when running "dse spark-submit" to set
 * options such as the Cassandra username, Cassandra password, split sizes, etc.
 * Do this as you would normally do with "dse spark-submit".
 *
 *
 * Example command-line: 
 *   dse spark-submit --conf spark.migrator.src.url="jdbc:mysql://1.2.3.4:5678/mydb" \
 *                    --conf spark.migrator.src.driver="com.mysql.jdbc.Driver" \
 *                    --conf spark.migrator.src.username=jdbcuser \
 *                    --conf spark.migrator.src.password=letmein \
 *                    --conf spark.migrator.src.table=mysqlTable \
 *                    --conf spark.migrator.src.partitionColumn=columnToCut \
 *                    --conf spark.migrator.src.lowerBound=0 \
 *                    --conf spark.migrator.src.upperBound=10000 \
 *                    --conf spark.migrator.src.numPartitions=50 \
 *                    --conf spark.migrator.dst.keyspace=cassandraKeyspace \
 *                    --conf spark.migrator.dst.table=cassandraTable \
 *                    --conf spark.migrator.src.columns="columnToCut,a,b,c,d" \
 *                    --jar /tmp/mysql-connector-java-5.1.40-bin.jar \
 *                    --class hessian.Migrator \
 *                    migrator-assembly-0.1.jar
 */

package hessian

import com.datastax.spark.connector._
import com.datastax.spark.connector.cql.CassandraConnector
import org.apache.spark.sql.hive.HiveContext
import org.apache.spark.{SparkConf, SparkContext}

object Migrator extends App {

  // Create the SparkConf, SparkContext, and HiveContext
  val conf = new SparkConf()
      .setAppName("JDBC-to-Cassandra Migrator");
  val sc = new SparkContext(conf);
  val sqlContext = new HiveContext(sc);

  // Get Migrator variables
  val srcUrl             = conf.get("spark.migrator.src.url");
  val srcDriver          = conf.get("spark.migrator.src.driver");
  val srcUsername        = conf.get("spark.migrator.src.username");
  val srcPassword        = conf.get("spark.migrator.src.password");
  val srcTable           = conf.get("spark.migrator.src.table");
  val srcPartitionColumn = conf.get("spark.migrator.src.partitionColumn");
  val srcLowerBound      = conf.get("spark.migrator.src.lowerBound");
  val srcUpperBound      = conf.get("spark.migrator.src.upperBound");
  val srcNumPartitions   = conf.get("spark.migrator.src.numPartitions", "5");
  val srcColumns         = conf.get("spark.migrator.src.columns", "*");

  val dstKeyspace        = conf.get("spark.migrator.dst.keyspace");
  val dstTable           = conf.get("spark.migrator.dst.table");

  /* We will do the migration in two steps.  The first step is to
   * create a Dataframe representing the data from the JDBC source.
   * The second step is to save that Dataframe to Cassandra.
   * The reason for two steps is that if you have a more specific
   * approach to reading from your JDBC source, then you can just
   * create the fromJdbc Dataframe in your more optimized way and 
   * still leverage step 2.
   */
  // Read data from JDBC source
  val fromJdbc = spark.read.
                       format("jdbc").
                       option("driver", srcDriver).
                       option("url", srcUrl).
                       option("user", srcUsername).
                       option("password", srcPassword).
                       option("dbtable", srcTable).
                       option("partitionColumn", srcPartitionColumn).
                       option("lowerBound", srcLowerBound).
                       option("upperBound", srcUpperBound).
                       option("numPartitions", srcNumPartitions).
                       load().select(columns);

  // Write data to Cassandra
  fromJdbc.write.format("org.apache.spark.sql.cassandra").
                 option("keyspace", dstKeyspace).
                 option("table", dstTable).save();

  // Shutdown and exit
  sc.stop()
  sys.exit(0)
}
